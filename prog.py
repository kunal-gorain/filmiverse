N = int(input())

X = []

for i in range(N):
    n = int(input())
    X.append(n)

Y = []
for i in range(N):
    n = int(input())
    Y.append(n)
    
uniques_val = []
uniques = []

for i in range(len(X)):
    uniques.append([i])
    uniques_val.append([X[i]])
    for j in range(i+1, len(X)):
        if X[j] not in uniques_val[i]:
            uniques_val[i].append(X[j])    
            uniques[i].append(j)
    
def find_Sum(values, array):
    sum = 0
    for each in values:
        sum += array[each]

    return sum

largest = 0
for each in uniques:
    add = find_Sum(each, Y)
    largest = max(largest, add)

print(largest)