## Project Developer:
Kunal Gorain, Gourango Sundor Mahato, Sagar Mahata, Jayanta Chandra, Ravi Shankar

## Project For:
BCREC APC - 5th Sem minor Project 

## Used technology:
Django (Python) + Jquery, Bootstrap

# Please follow the process below to install

### setting up a development environment
2. start an environment with requirements 
   e.g. pipenv install -r <project-folder>/requirements.txt

### migrating the already defined models and creating the super user
3. python manage.py migrate

### super user should be created before the dummy data be loaded!
4. python manage.py createsuperuser

### loading dummy data
5. python manage.py loaddata data.json

### collectstatic -> required/needed for ckeditor (wysiwyg editor) 
6. python manage.py collectstatic


