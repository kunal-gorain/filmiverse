pip install Django==3.0.7
pip install django-ckeditor==5.9.0
pip install django-crum==0.7.6
pip install django-heroku==0.3.1
pip install django-imagekit==4.0.2
pip install django-import-export==2.2.0
pip install djangorestframework==3.11.0
pip install Pillow==7.1.2
pip install Unidecode==1.1.1

# needed for heroku
# django-heroku==0.3.1

# needed for debugging purpose when debug False
django-debug-toolbar==2.2

# needed for postgreSQL
psycopg2-binary==2.8.5

# needed for deployment
gunicorn==20.0.4

# needed for automatic selenium tests
# selenium==3.141.0
